<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
            'name' => 'Super Admin',
            'email' => 'superadmin@admin.com',
            'password' => bcrypt('password')
            ],
            [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('password')
            ],
        ]);
    }
}
