<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('permissions')->insert([
            [
            'name' => 'create'
            ],
            [
            'name' => 'read'
            ],
            [
            'name' => 'update'
            ],
            [
            'name' => 'delete'
            ],
            [
            'name' => 'view'
            ],
        ]);

       DB::table('roles')->insert([
            [
            'name' => 'superadmin'
            ],
            [
            'name' => 'admin'
            ],
        ]);

        DB::table('admin_role')->insert([
            [
            'admin_id' => '1',
            'role_id' => '1'
            ],
            [
            'admin_id' => '2',
            'role_id' => '2'
            ],
        ]);

         DB::table('permission_role')->insert([
            [
            'permission_id' => '1',
            'role_id' => '1'
            ],
            [
            'permission_id' => '2',
            'role_id' => '1'
            ],
            [
            'permission_id' => '3',
            'role_id' => '1'
            ],
            [
            'permission_id' => '4',
            'role_id' => '1'
            ],
            [
            'permission_id' => '5',
            'role_id' => '1'
            ],
            [
            'permission_id' => '1',
            'role_id' => '2'
            ],
            [
            'permission_id' => '2',
            'role_id' => '2'
            ],
        ]);

    }
}
