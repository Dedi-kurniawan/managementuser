<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except(['adminLogout']);
    }

    public function showLoginForm()
    {
        return view('authAdmin.login');
    }

    public function login(Request $request)
    {
        // nek arep login karo email name di ganti email ae yo
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required|min:6',
        ]);

        $credential = [
            'name' => $request->name,
            'password' => $request->password
        ];
        if(Auth::guard('admin')->attempt($credential, $request->member)){
            return redirect()->intended(route('admin.home'));
        }
        return redirect()->back()->withInput($request->only('name', 'remember'));
    }

    public function adminLogout(Request $request)
    {
        Auth::guard('admin')->logout();

        // $request->session()->invalidate();

        return redirect()->route('admin.login');
        
    }
}
